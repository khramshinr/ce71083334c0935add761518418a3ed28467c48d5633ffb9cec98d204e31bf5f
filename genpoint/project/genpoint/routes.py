import json

from flask import request
from sqlalchemy.exc import SQLAlchemyError

from project import db

from . import genpoint_blueprint
from .utils import DecimalEncoder


@genpoint_blueprint.route('/ping')
def ping():
    return 'Pong'


@genpoint_blueprint.route('/', methods=['POST'])
def genpoint():
    print(request.data)
    function_json = json.loads(request.data.decode('utf8'))
    stmt = f'with src as(\
                select generate_series as t FROM GENERATE_SERIES({function_json["start"]}, {function_json["end"]}, {function_json["dt"]})\
            )\
            select t, {function_json["function"]} from src'

    try:
        return json.dumps([list(row) for row in db.engine.execute(stmt)], cls=DecimalEncoder)
    except SQLAlchemyError as err:
        return err.__str__(), 400
