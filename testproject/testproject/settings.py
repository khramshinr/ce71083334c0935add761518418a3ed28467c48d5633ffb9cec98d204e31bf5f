import sys

import environ

# Environment setup
# =================

BASE_DIR = environ.Path(__file__) - 2

env = environ.Env(DEBUG=(bool, False), ENVIRONMENT_NAME=(str, 'development'),)
ENVIRONMENT_FILE = env('ENVIRONMENT_FILE')
environ.Env.read_env(ENVIRONMENT_FILE)

ENVIRONMENT_NAME = env('ENVIRONMENT_NAME')  # Re-read again from the file
TEST_ENVIRONMENT = ENVIRONMENT_NAME in ['test'] or ENVIRONMENT_NAME.startswith('test_')
TESTING = len(sys.argv) > 1 and sys.argv[1] == 'test'
print('Current environment name is "{}".'.format(ENVIRONMENT_NAME), file=sys.stderr)
if TESTING and not TEST_ENVIRONMENT: print('ENVIRONMENT_NAME does not start with "test". Are you sure you have set the ENVIRONMENT_FILE variable correctly?', file=sys.stderr)
if TESTING: TESTING_HOST = 'testserver'

# Django settings
# ===============

SECRET_KEY = env('SECRET_KEY')
DEBUG = env('DEBUG')

ALLOWED_HOSTS = tuple(env.list('ALLOWED_HOSTS', default=[]))

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    # Local Apps
    'testproject',
    'function',
]

if ENVIRONMENT_NAME == 'development':
    INSTALLED_APPS += ['django_extensions']


MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'testproject.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'testproject.wsgi.application'


# Database
# https://docs.djangoproject.com/en/2.1/ref/settings/#databases

DATABASES = {
    'default': env.db(),
}
if TESTING:  # we support concurrent tests running in parallel
    DATABASES['default']['TEST'] = dict(NAME='{}_{}'.format(DATABASES['default']['NAME'], ENVIRONMENT_NAME))

# Celery
CELERY_BROKER_URL = env('CELERY_BROKER_URL')

# Password validation
# https://docs.djangoproject.com/en/2.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/2.1/topics/i18n/

LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'  # Always use UTC
USE_I18N = True
USE_L10N = True
USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.1/howto/static-files/

STATIC_ROOT = BASE_DIR(env('STATIC_ROOT', default='static-{ENVIRONMENT_NAME}').format(ENVIRONMENT_NAME=ENVIRONMENT_NAME))
MEDIA_ROOT = BASE_DIR(env('MEDIA_ROOT', default='media-{ENVIRONMENT_NAME}').format(ENVIRONMENT_NAME=ENVIRONMENT_NAME))
STATIC_URL = env('STATIC_URL', default='/static/')
MEDIA_URL = env('MEDIA_URL', default='/media/')

# Services
GENPOINT_URL = env('GENPOINT_URL')
GENGRAPH_URL = env('GENGRAPH_URL')
