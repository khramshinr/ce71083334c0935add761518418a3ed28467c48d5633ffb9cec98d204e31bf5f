import datetime
import json
import requests

from django.conf import settings
from django.core.files.base import ContentFile

from celery import task

from function.models import Func
from function.models import FuncStatus
from function.models import FuncParam


def _set_status_error(func, text_err):
    func.status = FuncStatus.objects.get(name='err')
    func.err_msg = text_err
    func.save()


@task()
def handle_function(func_id):
    print('run task')
    func = None
    while func is None:
        try:
            func = Func.objects.get(id=func_id)
        except Func.DoesNotExist:
            func = None

    func.status = FuncStatus.objects.get(name='in_progress')
    func.save()
    try:
        try:
            interval = func.interval
        except FuncParam.DoesNotExist:
            _set_status_error(func, 'Can\'t find function parameter: Interval')
            return

        try:
            dt = func.dt
        except FuncParam.DoesNotExist:
            _set_status_error(func, 'Can\'t find function parameter: dt')
            return

        request_data = {'function': func.func_txt.lstrip('y='),
                        'start': int((datetime.datetime.now()-datetime.timedelta(days=interval)).timestamp()/60/60),
                        'end': int(datetime.datetime.now().timestamp()/60/60),
                        'dt': dt,
                        }
        print('request to genpoint')
        r = requests.post(url=settings.GENPOINT_URL, json=request_data)
        print(r.status_code)
        if r.status_code == 200:
            request_data = {"infile":{"title": {"text": func.func_txt},
                                        "xAxis": {"categories": [x[0] for x in json.loads(r.text)]},
                                        "series": [{"data": [y[1] for y in json.loads(r.text)]}]
                                    }
                            }
            print('request to gengraph')
            r = requests.post(url=settings.GENGRAPH_URL, json=request_data)
            print(r.status_code)
            if r.status_code == 200:
                func.status = FuncStatus.objects.get(name='success')
                func.image.save(name='graph.jpg', content=ContentFile(r.content))
                func.save()
            else:
                _set_status_error(func, r.text)
                return

        else:
            _set_status_error(func, r.text)
            return

    except Exception as err:
        _set_status_error(func, err)
