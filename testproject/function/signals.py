from django.db.models.signals import post_save

from django.dispatch import receiver
from function.models import Func

from .tasks import handle_function


@receiver(post_save, sender=Func)
def func_created(sender, instance, created, **kwargs):
    print('signals')
    if created:
        handle_function.delay(instance.id)
