from django.contrib import admin

from .models import Func
from .models import FuncParam
from .models import FuncStatus
from .models import FuncType
from .models import FuncTypeParam
from .tasks import handle_function


def update_selected_funcs(modeladmin, request, queryset):
    for func in queryset.all():
        func.status = FuncStatus.objects.get(name='new')
        func.err_msg = ''
        func.image = ''
        func.save()
        handle_function.delay(func.id)


class FuncParamInline(admin.TabularInline):
    model = FuncParam
    extra = 0


class FuncTypeAdmin(admin.ModelAdmin):
    list_display = ('type', 'description')


class FuncTypeParamAdmin(admin.ModelAdmin):
    list_display = ('type', 'param_name', 'description')


class FuncStatusAdmin(admin.ModelAdmin):
    list_display = ('name', 'description')


class FuncAdmin(admin.ModelAdmin):
    inlines = [FuncParamInline]
    list_display = ('func_txt', 'description', 'image_img', 'interval', 'dt', 'last_handel_date', 'status')
    actions = [update_selected_funcs]


class FuncParamAdmin(admin.ModelAdmin):
    list_display = ('func', 'func_type_param', 'value')


admin.site.register(Func, FuncAdmin)
admin.site.register(FuncParam, FuncParamAdmin)
admin.site.register(FuncStatus, FuncStatusAdmin)
admin.site.register(FuncType, FuncTypeAdmin)
admin.site.register(FuncTypeParam, FuncTypeParamAdmin)

