from django.apps import AppConfig


class FunctionConfig(AppConfig):
    name = 'function'

    def ready(self):
        import function.signals
