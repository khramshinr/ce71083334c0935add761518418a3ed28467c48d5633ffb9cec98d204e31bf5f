from django.db import models

from testproject.storage import OverwriteStorage


def _generate_func_graph_upload_name(instance, filename):
    return '{}/{}'.format(instance.id, filename)


class FuncType(models.Model):
    type = models.CharField(max_length=64, help_text='Type name for function.')
    description = models.TextField(null=True, blank=True, help_text='Type description.')

    def __str__(self):
        return self.type


class FuncTypeParam(models.Model):
    type = models.ForeignKey('FuncType', on_delete=models.CASCADE, help_text='fk to fuction type.')
    param_name = models.CharField(max_length=64, help_text='Parametr name')
    description = models.TextField(null=True, blank=True, help_text='Type description.')

    def __str__(self):
        return self.param_name


class FuncStatus(models.Model):
    name = models.CharField(max_length=64, help_text='Type name for function.')
    description = models.TextField(null=True, blank=True, help_text='Type description.')

    def __str__(self):
        return self.name


class Func(models.Model):
    func_txt = models.CharField(max_length=64, help_text='Text view function.')
    type = models.ForeignKey('FuncType', on_delete=models.CASCADE, help_text='fk to fuction type.')
    status = models.ForeignKey('FuncStatus', on_delete=models.CASCADE, help_text='fk to status.')
    description = models.TextField(null=True, blank=True, help_text='Function description.')
    image = models.ImageField(null=True, blank=True, upload_to=_generate_func_graph_upload_name, storage=OverwriteStorage(), help_text='Graphic image function.')
    err_msg = models.TextField(null=True, blank=True, help_text='Error message text.')
    last_handel_date = models.DateTimeField(auto_now=True, help_text='Date time last handel function')

    def __str__(self):
        return self.func_txt

    @property
    def image_img(self):
        if self.image:
            from django.utils.safestring import mark_safe
            return mark_safe(u'<a href="{0}" target="_blank"><img src="{0}" width="200"/></a>'.format(self.image.url))
        else:
            return self.err_msg

    @property
    def interval(self):
        return FuncParam.objects.get(func=self, func_type_param=FuncTypeParam.objects.get(param_name='Interval')).value

    @property
    def dt(self):
        return FuncParam.objects.get(func=self, func_type_param=FuncTypeParam.objects.get(param_name='dt')).value

    image_img.fget.short_description = 'Chart'
    interval.fget.short_description = 'Interval t, Days'
    dt.fget.short_description = 'Increment dt, Hours'


class FuncParam(models.Model):
    func = models.ForeignKey('Func', on_delete=models.CASCADE, help_text='fk to fuction.')
    func_type_param = models.ForeignKey('FuncTypeParam', on_delete=models.CASCADE, help_text='fk to parametr.')
    value = models.IntegerField(help_text='Value parametr.')
